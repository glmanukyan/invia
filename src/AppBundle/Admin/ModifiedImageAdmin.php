<?php

namespace AppBundle\Admin;

use AppBundle\Entity\ModifiedImage;
use AppBundle\Entity\StockImage;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

class ModifiedImageAdmin extends AbstractAdmin
{

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name', null, [
                'show_filter' => true
            ])
            ->add('user', null, [
                'show_filter' => true
            ])
            ->add('stockImage', null, [
                'show_filter' => true
            ])
            ->add('createdAt','doctrine_orm_datetime', [
                'field_type'=>'sonata_type_date_picker'
            ])
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('Modified Image', null, [
                'template' => 'modifiedimage/admin/preview_image.html.twig',
                'modifiedImagesDirectory' => 'uploads/images/',
                'imageWidth' => '80'
            ])
            ->add('stockImage', StockImage::class, [
                'imageWidth' => '80',
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('user', User::class, [
                'label' => 'Created By',
                'route' => [
                    'name' => ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) ? 'show' : 'batch'
                ]
            ])
            ->add('name', TextType::class)
            ->add('createdAt', 'date')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }

    protected function configureFormFields(FormMapper $form)
    {
        if($this->isCurrentRoute('edit')) {
            $form
                ->add('file', FileType::class, [
                    'label' => 'Upload new image',
                    'required' => false,
                    'data_class' => null
                ])
                ->add('name')
                ->add('stockImage', ChoiceType::class, [
                    'choices' => $this->getStockImages()
                ])
            ;
        } else {
            $form
                ->add('file', FileType::class, [
                    'required' => true,
                    'data_class' => null
                ])
                ->add('name')
                ->add('stockImage', ChoiceType::class, [
                    'choices' => $this->getStockImages()
                ])
            ;
        }

    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('Modified Image', null, [
                'template' => 'modifiedimage/admin/preview_image.html.twig',
                'modifiedImagesDirectory' => 'uploads/images/',
                'imageWidth' => '80'
            ])
            ->add('stockImage', StockImage::class, [
                'imageWidth' => '80',
                'route' => [
                    'name' => 'show'
                ]
            ])
            ->add('user', User::class, [
                'label' => 'Created By',
                'route' => [
                    'name' => ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) ? 'show' : 'batch'
                ]
            ])
            ->add('name', TextType::class)
            ->add('createdAt', 'date')
        ;
    }


    public function prePersist($object)
    {
        $fileName = $this->manageFileUpload($object->getFile());

        $object->setSrc($fileName);

        $date = new \DateTime();
        $object->setUser($this->getUser());
        $object->setCreatedAt($date);

    }

    public function preUpdate($object)
    {
        if(! is_null($object->getFile())) {
            $fileName = $this->manageFileUpload($object->getFile());
            $object->setSrc($fileName);
        }

        $date = new \DateTime();
        $object->setUser($this->getUser());
        $object->setUpdatedAt($date);
    }


    protected function getUser()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }

    protected function getStockImages()
    {
        $repository = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(StockImage::class);

        $stockImages = $repository->findAll();

        $list = [];

        foreach ($stockImages as $image) {
            $list[$image->getName()] = $image ;
        }

        return $list;
    }

    private function manageFileUpload($image)
    {
        $file = array_shift($_FILES);

        if ($file['error']['file'] !== UPLOAD_ERR_OK) {
            throw new \Exception("Upload failed with error code " . $file['error']['file'],500);
        }

        $info = getimagesize($file['tmp_name']['file']);
        if ($info === FALSE) {
            throw new \Exception("Unable to determine image type of uploaded file", 500);
        }

        if (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
            throw new \Exception("Not a gif/jpeg/png", 500);
        }

        $fileName = $this->generateUniqueFileName().'.'.$image->guessExtension();
        $path = $this->getParameter('kernel.project_dir') . ModifiedImage::MODIFIED_IMAGES_DIRECTORY;
        $image->move( $path, $fileName );

        return $fileName;

    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    protected function getParameter($name)
    {
        return $this->getConfigurationPool()->getContainer()->getParameter($name);
    }
}