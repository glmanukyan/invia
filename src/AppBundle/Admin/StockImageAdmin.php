<?php

namespace AppBundle\Admin;

use AppBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;

class StockImageAdmin extends AbstractAdmin
{
    public function getDashboardActions()
    {
        $actions = parent::getDashboardActions();

        if(! ($this->getUser()->hasRole('ROLE_SUPER_ADMIN'))) {
            unset($actions['create']);
        }

        return $actions;
    }

    public function getBatchActions()
    {
        $actions = parent::getBatchActions();
        if(! ($this->getUser()->hasRole('ROLE_SUPER_ADMIN'))) {
            unset($actions['delete']);
        }
        return $actions;
    }

    public function getActionButtons($action, $object = null)
    {
        $actions =  parent::getActionButtons($action, $object);

        if(! ($this->getUser()->hasRole('ROLE_SUPER_ADMIN'))) {
            unset($actions['create']);
        }

        return $actions;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name', null, [
                'show_filter' => true
            ])
            ->add('user', null, [
                'show_filter' => true
            ])
            ->add('createdAt','doctrine_orm_datetime', [
                'field_type'=>'sonata_type_date_picker'
            ])
            ->add('licenseEndDate','doctrine_orm_datetime', [
                'field_type'=>'sonata_type_date_picker',
                'show_filter' => true
            ])
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $this->getRoutes()->remove('delete');

        if($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) {
            $list
                ->add('Image', null, [
                    'template' => 'stockimage/admin/preview_image.html.twig',
                    'imageWidth' => '80'
                ])
                ->add('name', TextType::class)
                ->add('user', User::class, [
                    'label' => 'Created By',
                    'route' => [
                        'name' => 'show'
                    ]
                ])
                ->add('deeplink', UrlType::class)
                ->add('createdAt', 'date')
                ->add('licenseEndDate', 'date')
                ->add('_action', null, [
                    'actions' => [
                        'show' => [],
                        'edit' => [],
                        'delete' => [],
                    ]
                ])
            ;
        } else {
            $list
                ->add('Image', null, [
                    'template' => 'stockimage/admin/preview_image.html.twig',
                    'imageWidth' => '80'
                ])
                ->add('name', TextType::class)
                ->add('user', User::class, [
                    'label' => 'Created By',
                    'route' => [
                        'name' => 'batch'
                    ]
                ])
                ->add('deeplink', UrlType::class)
                ->add('createdAt', 'date')
                ->add('licenseEndDate', 'date')
                ->add('_action', null, [
                    'actions' => [
                        'show' => [],
                    ]
                ])
            ;
        }

    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('name', TextType::class)
            ->add('deeplink', UrlType::class)
            ->add('licenseEndDate', DatePickerType::class)
        ;
    }

    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('Image', null, [
                'template' => 'stockimage/admin/preview_image.html.twig',
                'imageWidth' => '350'
            ])
            ->add('name', TextType::class)
            ->add('user', User::class, [
                'route' => [
                    'name' => ($this->getUser()->hasRole('ROLE_SUPER_ADMIN')) ? 'show' : 'batch'

                ]
            ])
            ->add('deeplink', UrlType::class)
            ->add('createdAt', 'date')
            ->add('licenseEndDate', 'date')
        ;
    }

    public function prePersist($object)
    {
        $date = new \DateTime();
        $object->setUser($this->getUser());
        $object->setCreatedAt($date);
    }

    public function preUpdate($object)
    {
        $date = new \DateTime();

        $object->setUser($this->getUser());
    }

    protected function getUser()
    {
        return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
    }
}