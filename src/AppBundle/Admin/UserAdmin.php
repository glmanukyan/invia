<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('firstname', null, [
                'show_filter' => true
            ])
            ->add('lastname', null, [
                'show_filter' => true
            ])
            ->add('username', null, [
                'show_filter' => false
            ])
            ->add('email', null, [
                'show_filter' => true
            ])
            ->add('roles',null, [
                'show_filter' => true
            ])
        ;
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('firstname')
            ->add('lastname')
            ->add('email')
            ->add('username')
            ->add('roles', null, [
                'template' => 'user/admin/roles.html.twig',
            ])
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ]
            ])
        ;
    }


    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add('firstname')
            ->add('lastname')
            ->add('username')
            ->add('email')
            ->add('plainPassword', PasswordType::class,
                [
                    'label' => 'Password',
                    'required' => ($this->isCurrentRoute('edit')) ? false : true,
                    'attr' => [
                        'placeholder' => ($this->isCurrentRoute('edit')) ? 'Leave blank if do not want to change password' : '',
                    ]
                ]
            )
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Admin' => 'ROLE_ADMIN',
                    'Super Admin' => 'ROLE_SUPER_ADMIN'
                ],
                'multiple' => true,
            ])
        ;
    }


    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('id')
            ->add('firstname')
            ->add('lastname')
            ->add('username')
            ->add('email')
            ->add('roles', null, [
                'template' => 'user/admin/roles_show.html.twig',
            ])        ;
    }

    public function prePersist($object)
    {
        $object->setEnabled(1);
        $userManager = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $userManager->updateUser($object, true);
    }

    public function preUpdate($object)
    {
        if($object->getPlainPassword()) {
            $userManager = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
            $userManager->updateUser($object, true);
        }
    }


}
