<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModifiedImage
 *
 * @ORM\Table(name="modified_images")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ModifiedImageRepository")
 */
class ModifiedImage
{
    const MODIFIED_IMAGES_DIRECTORY = '/web/uploads/images';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="stockImages")
     *
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $user;

    /**
     * @var \AppBundle\Entity\StockImage
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StockImage", inversedBy="modifiedImages")
     *
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stock_image_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $stockImage;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=180)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="src", type="string", length=180)
     */
    private $src;

    private $file;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


    public function __toString()
    {
        return ($this->name) ? $this->name : 'null';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $user
     * @return StockImage
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param StockImage $stockImage
     * @return StockImage
     */
    public function setStockImage($stockImage)
    {
        $this->stockImage = $stockImage;
        return $this;
    }

    /**
     * @return StockImage
     */
    public function getStockImage()
    {
        return $this->stockImage;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ModifiedImage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set src
     *
     * @param string $src
     *
     * @return ModifiedImage
     */
    public function setSrc($src)
    {
        $this->src = $src;

        return $this;
    }

    /**
     * Get src
     *
     * @return string
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return ModifiedImage
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ModifiedImage
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $file
     * @return ModifiedImage
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }
}
