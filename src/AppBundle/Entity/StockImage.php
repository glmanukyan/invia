<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * StockImage
 *
 * @ORM\Table(name="stock_images")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StockImageRepository")
 */
class StockImage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="stockImages")
     *
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=180)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="deeplink", type="string", length=180)
     */
    private $deeplink;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="license_end_date", type="datetime")
     */
    private $licenseEndDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ModifiedImage", mappedBy="stockImage")
     */
    private $modifiedImages;


    /**
     * StockImage constructor.
     */
    public function __construct()
    {
        $this->modifiedImages = new ArrayCollection();
    }

    public function __toString()
    {
        return ($this->name) ? $this->name : 'null';
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param User $user
     * @return StockImage
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StockImage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deeplink
     *
     * @param string $deeplink
     *
     * @return StockImage
     */
    public function setDeeplink($deeplink)
    {
        $this->deeplink = $deeplink;

        return $this;
    }

    /**
     * Get deeplink
     *
     * @return string
     */
    public function getDeeplink()
    {
        return $this->deeplink;
    }

    /**
     * Set licenseEndDate
     *
     * @param \DateTime $licenseEndDate
     *
     * @return StockImage
     */
    public function setLicenseEndDate($licenseEndDate)
    {
        $this->licenseEndDate = $licenseEndDate;

        return $this;
    }

    /**
     * Get licenseEndDate
     *
     * @return \DateTime
     */
    public function getLicenseEndDate()
    {
        return $this->licenseEndDate;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return StockImage
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return StockImage
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getModifiedImages()
    {
        return $this->modifiedImages;
    }
}
